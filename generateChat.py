from chatbot import chatbot


def generate_chat():
    list = []
    while True:
        text = input("Enter your message ")
        if text == "quit":
            break
        else:
            message = {"role": "user", "content": text}
            response = chatbot(message)
            print(response)
            list.append(message)
