from openai import OpenAI
import os


client = OpenAI(
    # This is the default and can be omitted
    api_key=os.environ.get("OPENAI_API_KEY"),
)


def chatbot(message, list = []):
    list.append(message)
    response = client.chat.completions.create(
        model='gpt-3.5-turbo',
        messages=list
    )
    print(response.choices)
    return response.choices[0].message.content
